package com.example.test.myapplication;

import android.app.Activity;
import android.databinding.DataBindingUtil;

import com.example.test.myapplication.databinding.ActivityMainBinding;

public class ListController extends Activity {
    public MainActivity context;

    public ListController(MainActivity mainActivity) {
        this.context = mainActivity;
    }

    public void showCells(final NetworkManager listItems) {

        ActivityMainBinding binding = DataBindingUtil.setContentView(context, R.layout.activity_main);
        binding.setItems(listItems);

    }
}