package com.example.test.myapplication;

import android.databinding.ObservableArrayList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NetworkManager {
    ArrayList<Item> items;
    Integer amount;

    private static NetworkManager mInstance;
    private String ids = "items?ids=";

    public static NetworkManager getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkManager();
        }
        return mInstance;
    }

    public ObservableArrayList<Item> list = new ObservableArrayList<>();

    public void loadItem(int amount) {
        this.amount = amount;
        items = new ArrayList<>();
        int i;
        int idNumber = 0;

        for (i = list.size(); i < 75; i++) {
            idNumber++;
            ids = ids + Integer.toString(idNumber) + ",";
        }

        App.getApi().getItems(ids).enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                list.addAll(response.body());
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                System.out.println("_____УПС");
            }
        });
    }
}
