package com.example.test.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    ListController listController = new ListController(this);
    NetworkManager networkManager = NetworkManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        networkManager.loadItem(100);

        startBroadcastListener();

        listController.showCells(networkManager);
    }

    public void showDetail(int position, Context context) {

        Intent intent = new Intent(context, DetailController.class);
        intent.putExtra("item", networkManager.list.get(position));
        context.startActivity(intent);
    }

    private void startBroadcastListener() {
        IntentFilter intentFilter = new IntentFilter("BROADCAST_ListAdapter_FOR_MainActivity");
        BroadcastReceiver mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                showDetail(Integer.parseInt(intent.getStringExtra("position")), context);
            }
        };
        registerReceiver(mReceiver, intentFilter);
    }
}
