package com.example.test.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface APIService {
    @GET("v2/{itemsIds}")
    Call<List<Item>> getItems(
            @Path("itemsIds") String itemsIds);
}
