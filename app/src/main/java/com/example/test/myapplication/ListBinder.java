package com.example.test.myapplication;

import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.widget.ImageView;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

public class ListBinder {

    @BindingAdapter({"bind:imageRes"})
    public static void loadImage(ImageView view, String icon) {
        Picasso.with(view.getContext())
                .load(icon)
                .placeholder(R.drawable.time)
                .into(view);
    }

    @BindingAdapter("bind:items")
    public static void bindList(ListView view, ObservableArrayList<Item> list) {
        ListAdapter adapter = new ListAdapter(list);
        view.setAdapter(adapter);
    }
}