package com.example.test.myapplication;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.test.myapplication.databinding.NewListBinding;

public class ListAdapter extends BaseAdapter {
    private ObservableArrayList<Item> list;
    private LayoutInflater inflater;

    public ListAdapter(ObservableArrayList<Item> listItems) {
        this.list = listItems;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        NewListBinding binding = DataBindingUtil.inflate(inflater, R.layout.new_list, parent, false);
        binding.setItem(list.get(position));

        binding.setClicker(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPositionForMainActivity(convertView, position);
            }
        });

        return binding.getRoot();
    }

    public void sendPositionForMainActivity(View convertView, int position) {
        Intent intent = new Intent("BROADCAST_ListAdapter_FOR_MainActivity");
        intent.putExtra("position", String.valueOf(position));
        convertView.getContext().sendBroadcast(intent);
    }
}