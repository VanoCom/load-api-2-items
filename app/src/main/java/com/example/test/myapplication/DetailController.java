package com.example.test.myapplication;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.test.myapplication.databinding.ActivityDetailBinding;

public class DetailController extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Item item = (Item) getIntent().getSerializableExtra("item");

        ActivityDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        binding.setItem(item);
    }

    public void onBackClick(View view) {
        finish();
    }
}