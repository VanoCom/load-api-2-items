package com.example.test.myapplication;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

public class ItemAdapter {

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView imageView, String icon) {
        if (!icon.equals("")) {
            Picasso.with(imageView.getContext())
                    .load(icon)
                    .placeholder(R.drawable.time)
                    .into(imageView);
        }
    }
}